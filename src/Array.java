
import java.util.Arrays;
import java.util.Collections;

public class Array {

    public static void main(String[] args) {
        Integer[] arr = {20, 10, 40, 60, 30, 70, 50};
        Arrays.sort(arr);
        
        System.out.println("The ascending order of array: "
                + Arrays.toString(arr)); //ascending order

        Arrays.sort(arr, Collections.reverseOrder());
        System.out.println("The descending order of array: "
                + Arrays.toString(arr));//descending order

        char[] name = {'b', 'a', 'd', 'c', 'f', 'e'};
        Arrays.sort(name);
        System.out.println("The ascending order of name");
        System.out.println(name);
        
    }
}
