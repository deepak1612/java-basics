
import java.util.Scanner;

public class UniversityExam {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Your Name: ");
        String name = input.next();

        System.out.println("Hi " + name + " Please Enter Your Department "
                + "1.For Computer Science, "
                + "2.For Mechanical, "
                + "3.For EEE, "
                + "4.For Civil");

        int department = input.nextInt();
        int year;

        switch (department) {

            case 1:
                System.out.println("Enter your year : ");
                year = input.nextInt();
                if (year == 1) {
                    System.out.println("Your exam date is June 1 to June 20");
                } else if (year == 2) {
                    System.out.println("Your exam date is June 10 to June 25");
                } else if (year == 3) {
                    System.out.println("Your exam date is June 7 to June 19");
                } else if (year == 4) {
                    System.out.println("Your exam date is June 3 to June 10");
                } else {
                    System.out.println("Enter the correct year");
                }
                break;

            case 2:
                System.out.println("Enter your year : ");
                year = input.nextInt();
                if (year == 1) {
                    System.out.println("Your exam date is March 1 to March 20");
                } else if (year == 2) {
                    System.out.println("Your exam date is March 10 to March 25");
                } else if (year == 3) {
                    System.out.println("Your exam date is March 7 to March 19");
                } else if (year == 4) {
                    System.out.println("Your exam date is March 3 to March 10");
                } else {
                    System.out.println("Enter the correct year");
                }
                break;

            case 3:
                System.out.println("Enter your year : ");
                year = input.nextInt();
                if (year == 1) {
                    System.out.println("Your exam date is April 1 to April 20");
                } else if (year == 2) {
                    System.out.println("Your exam date is April 10 to April 25");
                } else if (year == 3) {
                    System.out.println("Your exam date is April 7 to April 19");
                } else if (year == 4) {
                    System.out.println("Your exam date is April 3 to April 10");
                } else {
                    System.out.println("Enter the correct year");
                }
                break;

            case 4:
                System.out.println("Enter your year : ");
                year = input.nextInt();
                if (year == 1) {
                    System.out.println("Your exam date is July 1 to July 20");
                } else if (year == 2) {
                    System.out.println("Your exam date is July 10 to July 25");
                } else if (year == 3) {
                    System.out.println("Your exam date is July 7 to July 19");
                } else if (year == 4) {
                    System.out.println("Your exam date is July 3 to July 10");
                } else {
                    System.out.println("Enter the correct year");
                }
                break;
            default:
                System.out.println("Enter the correct option");

        }
    }

}
