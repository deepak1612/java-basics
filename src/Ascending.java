
public class Ascending {
    public static void main(String args[]) {
        int[] a = {7, 3, 8, 1, 6, 9, 30, 22, 10};
        int i, j, temp = 0;

 

        for (i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        for (i = 0; i < a.length; i++) {
            for (j = i + 1; j < a.length; j++) {
                if (a[i] > a[j]) {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        System.out.println();
        System.out.println("sorted");
        for (i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }    
}
